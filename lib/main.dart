import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_delivery_app/blocs/autocomplete/autocomplete_bloc.dart';
import 'package:food_delivery_app/blocs/basket/basket_bloc.dart';
import 'package:food_delivery_app/blocs/filters/filters_bloc.dart';
import 'package:food_delivery_app/blocs/geolocation/geolocation_bloc.dart';
import 'package:food_delivery_app/blocs/place/place_bloc.dart';
import 'package:food_delivery_app/config/app_router.dart';
import 'package:food_delivery_app/config/theme.dart';
import 'package:food_delivery_app/repositories/geolocation/geolocation_repository.dart';
import 'package:food_delivery_app/repositories/places/places_repository.dart';
import 'package:food_delivery_app/screens/home/home_screen.dart';
import 'package:food_delivery_app/screens/location/location_screen.dart';
import 'package:food_delivery_app/screens/restaurant_details/restaurant_details_screen.dart';
import 'package:food_delivery_app/screens/restaurant_listing/restaurant_listing_screen.dart';
import 'package:food_delivery_app/simply_bloc_observer.dart';

void main() {
  BlocOverrides.runZoned(() {
    runApp(const MyApp());
  }, blocObserver: SimpleBlocObserver());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<GeolocationRepository>(
          create: (_) => GeolocationRepository(),
        ),
        RepositoryProvider<PlaceRepository>(
          create: (_) => PlaceRepository(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => GeolocationBloc(
                  geolocationRepository: context.read<GeolocationRepository>())
                ..add(LoadGeolocation())),
          BlocProvider(
              create: (context) => AutocompleteBloc(
                  placeRepository: context.read<PlaceRepository>())
                ..add(LoadAutocomplete())),
          BlocProvider(
              create: (context) =>
                  PlaceBloc(placeRepository: context.read<PlaceRepository>())),
          BlocProvider(create: (context) => FilterBloc()..add(LoadFilter())),
          BlocProvider(create: (context) => BasketBloc()..add(StartBasket()))
        ],
        child: MaterialApp(
          title: 'Food Delivery',
          theme: theme(),
          onGenerateRoute: AppRouter.onGenerationRout,
          initialRoute: HomeScreen.routName,
        ),
      ),
    );
  }
}
