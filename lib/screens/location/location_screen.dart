import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:food_delivery_app/blocs/autocomplete/autocomplete_bloc.dart';
import 'package:food_delivery_app/blocs/geolocation/geolocation_bloc.dart';
import 'package:food_delivery_app/model/place_model.dart';
import 'package:food_delivery_app/model/place_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../blocs/place/place_bloc.dart';

class LocationScreen extends StatelessWidget {
  const LocationScreen({Key? key}) : super(key: key);

  static const String routName = '/location';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => LocationScreen(),
        settings: RouteSettings(name: routName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PlaceBloc, PlaceState>(
        builder: (context, state) {
          if (state is PlaceLoading) {
            return Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  child: BlocBuilder<GeolocationBloc, GeolocationState>(
                    builder: (context, state) {
                      if (state is GeolocationLoading) {
                        return Center(child: CircularProgressIndicator());
                      } else if (state is GeolocationLoaded) {
                        return GoogleMap(
                          myLocationEnabled: true,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(state.position.latitude,
                                  state.position.longitude),
                              zoom: 10),
                        );
                      } else {
                        return Text('Something went wrong!');
                      }
                    },
                  ),
                ),
                Location(),
                SaveButton(),
              ],
            );
          } else if (state is PlaceLoaded) {
            return Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  child: GoogleMap(
                    myLocationEnabled: true,
                    initialCameraPosition: CameraPosition(
                        target: LatLng(state.place.lat, state.place.lng),
                        zoom: 10),
                  ),
                ),
                Location(),
                SaveButton(),
              ],
            );
          } else {
            return Text('Sometging went wrong!');
          }
        },
      ),
    );
  }
}

class Location extends StatelessWidget {
  const Location({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 50,
        left: 20,
        right: 20,
        child: Container(
          //   height: 400,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset(
                'assets/logo.svg',
                height: 50,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                  child: Column(
                children: [
                  LocationSearchBox(),
                  BlocBuilder<AutocompleteBloc, AutocompleteState>(
                    builder: (context, state) {
                      if (state is AutocompleteLoading) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is AutocompleteLoaded) {
                        return Container(
                          margin: const EdgeInsets.all(8),
                          height: 300,
                          color: state.autocomplete.isNotEmpty
                              ? Colors.black.withOpacity(0.6)
                              : Colors.transparent,
                          child: ListView.builder(
                            itemCount: state.autocomplete.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                title: Text(
                                  state.autocomplete[index].description,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(color: Colors.white),
                                ),
                                onTap: () {
                                  print('ontap');
                                  context.read<PlaceBloc>().add(LoadPlace(
                                      placeId:
                                          state.autocomplete[index].placeId));
                                },
                              );
                            },
                          ),
                        );
                      } else {
                        return Text('Error');
                      }
                    },
                  )
                ],
              )),
            ],
          ),
        ));
  }
}

class SaveButton extends StatelessWidget {
  const SaveButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 50,
        left: 20,
        right: 20,
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 70),
            child: ElevatedButton(
              child: Text('Save'),
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor),
            )));
  }
}

class LocationSearchBox extends StatelessWidget {
  const LocationSearchBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AutocompleteBloc, AutocompleteState>(
        builder: (context, state) {
      if (state is AutocompleteLoading) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      } else if (state is AutocompleteLoaded) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: 'Enter your location',
              suffixIcon: const Icon(Icons.search),
              contentPadding: EdgeInsets.only(left: 20, bottom: 5, right: 5),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.white),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (value) {
              context
                  .read<AutocompleteBloc>()
                  .add(LoadAutocomplete(searchInput: value));
            },
          ),
        );
      } else {
        return const Text('Something went wrong');
      }
    });
  }
}
