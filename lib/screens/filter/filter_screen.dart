import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_delivery_app/model/category_model.dart';
import 'package:food_delivery_app/model/filter_model.dart';
import 'package:food_delivery_app/model/restaurant_model.dart';

import '../../blocs/filters/filters_bloc.dart';
import '../../model/price_model.dart';
import '../../widgets/custom_category_filter.dart';
import '../../widgets/custom_price_filter.dart';

class FilterScreen extends StatelessWidget {
  const FilterScreen({Key? key}) : super(key: key);

  static const String routName = '/filters';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => FilterScreen(),
        settings: const RouteSettings(name: routName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Filter'),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BlocBuilder<FilterBloc, FilterState>(
                  builder: (context, state) {
                    if (state is FilterLoading) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    if (state is FilterLoaded) {
                      return ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).primaryColorDark,
                              shape: RoundedRectangleBorder(),
                              padding: EdgeInsets.symmetric(horizontal: 50)),
                          onPressed: () {
                            var categories = state.filter.categoryFilters
                                .where((filter) => filter.value)
                                .map((filter) => filter.category.name)
                                .toList();
                            var prices = state.filter.priceFilters
                                .where((filter) => filter.value)
                                .map((filter) => filter.price.price)
                                .toList();

                            List<Restaurant> restaurants = Restaurant
                                .restaurants
                                .where((restaurant) => categories.any(
                                    (category) =>
                                        restaurant.tags.contains(category)))
                                .where((restaurant) => prices.any((price) =>
                                    restaurant.priceCategory.contains(price)))
                                .toList();

                            Navigator.pushNamed(context, '/restaurant-listing',
                                arguments: restaurants);
                          },
                          child: Text('Apply'));
                    } else {
                      return Text('Something went wrong!');
                    }
                  },
                )
              ],
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Price',
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Theme.of(context).primaryColorDark),
              ),
              CustomPriceFilter(),
              Text(
                'Category',
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Theme.of(context).primaryColorDark),
              ),
              CustomCategoryFilter()
            ],
          ),
        ));
  }
}
