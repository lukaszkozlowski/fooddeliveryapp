import 'package:flutter/material.dart';
import 'package:food_delivery_app/widgets/restaurant_card.dart';

import '../../model/restaurant_model.dart';

class RestaurantListingScreen extends StatelessWidget {
  final List<Restaurant> restaurants;
  const RestaurantListingScreen({Key? key,required this.restaurants }) : super(key: key);

  static const String routName = '/restaurant-listing';

  static Route route({required List<Restaurant> restaurants }) {
    return MaterialPageRoute(
        builder: (_) => RestaurantListingScreen(restaurants: restaurants,), settings: RouteSettings(name: routName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Restaurants'),
      ),
      body: ListView.builder(itemCount: restaurants.length,itemBuilder: (context,index){
        return RestaurantCard(restaurant: restaurants[index]);
      })
    );
  }
}
