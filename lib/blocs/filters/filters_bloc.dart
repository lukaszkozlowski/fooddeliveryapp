import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:food_delivery_app/model/category_filter_model.dart';
import 'package:food_delivery_app/model/filter_model.dart';
import 'package:food_delivery_app/model/price_filter_model.dart';
import 'package:food_delivery_app/model/price_model.dart';

part 'filters_event.dart';

part 'filters_state.dart';

class FilterBloc extends Bloc<FilterEvent, FilterState> {
  FilterBloc() : super(FilterLoading()) {
    on<LoadFilter>(_onLoadFilter);
    on<UpdateCategoryFilter>(_onUpdateCategoryFilter);
    on<UpdatePriceFilter>(_onUpdatePriceFilter);
  }

  void _onLoadFilter(LoadFilter event, Emitter<FilterState> emit) {
    emit(FilterLoaded(
      filter: Filter(
        categoryFilters: CategoryFilter.filters,
        priceFilters: PriceFilter.filters,
      ),
    ));
  }

  void _onUpdateCategoryFilter(
      UpdateCategoryFilter event, Emitter<FilterState> emit) {
    final state = this.state;
    if (state is FilterLoaded) {
      final List<CategoryFilter> updatedCategoryFilters =
          state.filter.categoryFilters.map((categoryFilter) {
        return categoryFilter.id == event.categoryFilter.id
            ? event.categoryFilter
            : categoryFilter;
      }).toList();
      emit(FilterLoaded(
        filter: Filter(
          categoryFilters: updatedCategoryFilters,
          priceFilters: state.filter.priceFilters,
        ),
      ));
    }
  }

  void _onUpdatePriceFilter(
      UpdatePriceFilter event, Emitter<FilterState> emit) {
    final state = this.state;
      if (state is FilterLoaded) {
    final List<PriceFilter> updatedPriceFilters =
    state.filter.priceFilters.map((priceFilter) {
      return priceFilter.id == event.priceFilter.id
          ? event.priceFilter
          : priceFilter;
    }).toList();
     emit(FilterLoaded(
      filter: Filter(
        categoryFilters: state.filter.categoryFilters,
        priceFilters: updatedPriceFilters,
      ),
    ));
  }
  }

// @override
// Stream<FilterState> mapEventToState(FilterEvent event) async*{
//   if(event is LoadFilter){
//     yield* _mapFilterLoadToState();
//   }
//   if(event is UpdateCategoryFilter){
//     yield* _mapCategoryFilterUpdatedToState(event,state);
//   }
//   if(event is UpdatePriceFilter){
//     yield* _mapPriceFilterUpdatedToState(event,state);
//   }
// }

// Stream<FilterState> _mapFilterLoadToState() async* {
//   yield FilterLoaded(
//     filter: Filter(
//       categoryFilters: CategoryFilter.filters,
//       priceFilters: PriceFilter.filters,
//     ),
//   );
// }
//
// Stream<FilterState> _mapCategoryFilterUpdatedToState(
//     UpdateCategoryFilter event,
//     FilterState state,
//     ) async* {
//   if (state is FilterLoaded) {
//     final List<CategoryFilter> updatedCategoryFilters =
//     state.filter.categoryFilters.map((categoryFilter) {
//       return categoryFilter.id == event.categoryFilter.id
//           ? event.categoryFilter
//           : categoryFilter;
//     }).toList();
//     yield FilterLoaded(
//       filter: Filter(
//         categoryFilters: updatedCategoryFilters,
//         priceFilters: state.filter.priceFilters,
//       ),
//     );
//   }
// }
//
// Stream<FilterState> _mapPriceFilterUpdatedToState(
//     UpdatePriceFilter event,
//     FilterState state,
//     ) async* {
//   if (state is FilterLoaded) {
//     final List<PriceFilter> updatedPriceFilters =
//     state.filter.priceFilters.map((priceFilter) {
//       return priceFilter.id == event.priceFilter.id
//           ? event.priceFilter
//           : priceFilter;
//     }).toList();
//     yield FilterLoaded(
//       filter: Filter(
//         categoryFilters: state.filter.categoryFilters,
//         priceFilters: updatedPriceFilters,
//       ),
//     );
//   }
// }
}
