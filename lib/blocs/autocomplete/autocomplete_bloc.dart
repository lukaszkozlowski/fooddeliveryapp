import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:food_delivery_app/model/place_autocomplete_model.dart';
import 'package:food_delivery_app/repositories/places/places_repository.dart';

part 'autocomplete_event.dart';

part 'autocomplete_state.dart';

class AutocompleteBloc extends Bloc<AutocompleteEvent, AutocompleteState> {
  final PlaceRepository _placeRepository;
  StreamSubscription? _placeSubscrition;

  AutocompleteBloc({required PlaceRepository placeRepository})
      : _placeRepository = placeRepository,
        super(AutocompleteLoading());

  @override
  Stream<AutocompleteState> mapEventToState(AutocompleteEvent event) async* {
    if (event is LoadAutocomplete) {
      yield* _mapLoadAutocompleteToState(event);
    }
  }
 Stream<AutocompleteState> _mapLoadAutocompleteToState(LoadAutocomplete event) async*{
    _placeSubscrition?.cancel();

    final List<PlaceAutocomplete> autocomplete = await _placeRepository.getAutocomplate(event.searchInput);
    yield AutocompleteLoaded(autocomplete: autocomplete);
 }
}
