import 'package:flutter/material.dart';
import 'package:food_delivery_app/widgets/restaurant_tags.dart';

import '../model/restaurant_model.dart';

class RestaurantInformation extends StatelessWidget {
  final Restaurant restaurant;

  const RestaurantInformation({Key? key, required this.restaurant})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            restaurant.name,
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: Theme.of(context).primaryColorDark),
          ),
          SizedBox(
            height: 5,
          ),
          RestaurantTags(restaurant: restaurant),
          SizedBox(
            height: 5,
          ),
          Text(
            '${restaurant.distance}km away - \$${restaurant.deliveryFee} delivery Fee',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          SizedBox(height: 5,),
          Text(
            'Restaurant information',
            style: Theme.of(context).textTheme.headline5,
          ),
          Text(
            'aaaaaa vbbbb dfsdfaaaaaa vbbbb dfsdfaaaaaa vbbbb dfsdfaaaaaa vbbbb dfsdfaaaaaa vbbbb dfsdfaaaaaa vbbbb dfsdf',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }
}
