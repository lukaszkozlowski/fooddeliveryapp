import 'package:food_delivery_app/model/place_autocomplete_model.dart';

import '../../model/place_model.dart';

abstract class BasePlacesRepository {
  Future<List<PlaceAutocomplete>?> getAutocomplate(String searchInput) async {}

  Future<Place?> getPlace(String placeId) async {}
}
