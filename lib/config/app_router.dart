import 'package:flutter/material.dart';
import 'package:food_delivery_app/model/restaurant_model.dart';
import 'package:food_delivery_app/screens/basket/basket_screen.dart';
import 'package:food_delivery_app/screens/checkout/chekout_screen.dart';
import 'package:food_delivery_app/screens/delivery_time/delivery_time_screen.dart';
import 'package:food_delivery_app/screens/filter/filter_screen.dart';
import 'package:food_delivery_app/screens/home/home_screen.dart';
import 'package:food_delivery_app/screens/location/location_screen.dart';
import 'package:food_delivery_app/screens/restaurant_details/restaurant_details_screen.dart';
import 'package:food_delivery_app/screens/restaurant_listing/restaurant_listing_screen.dart';
import 'package:food_delivery_app/screens/voucher/voucher_screen.dart';

import '../screens/edit_basket/edit_basket_screen.dart';

class AppRouter {
  static Route onGenerationRout(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return HomeScreen.route();
      case HomeScreen.routName:
        return HomeScreen.route();
      case LocationScreen.routName:
        return LocationScreen.route();
      case BasketScreen.routName:
        return BasketScreen.route();
      case EditBasketScreen.routName:
        return EditBasketScreen.route();
      case CheckoutScreen.routName:
        return CheckoutScreen.route();
      case DeliveryTimeScreen.routName:
        return DeliveryTimeScreen.route();
      case FilterScreen.routName:
        return FilterScreen.route();
      case RestaurantDetailsScreen.routName:
        return RestaurantDetailsScreen.route(restaurant: settings.arguments as Restaurant);
      case RestaurantListingScreen.routName:
        return RestaurantListingScreen.route(restaurants: settings.arguments as List<Restaurant>);
      case VoucherScreen.routName:
        return VoucherScreen.route();
      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: Text('error'),
              ),
            ),
        settings: RouteSettings(name: '/error'));
  }
}
